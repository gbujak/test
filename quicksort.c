#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>
#include <time.h>

void swap(int* a, int* b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

int partition(int* arr, int lo, int hi) {
    int pivot = arr[hi];
    int pivot_index = lo;
    for (int i = lo; i < hi; ++i)
        if (arr[i] < pivot)
            swap(arr + (pivot_index++), arr + i);
    swap(arr + pivot_index, arr + hi);
    return pivot_index;
}

void quicksort(int* arr, int lo, int hi) {
    if (lo < hi) {
        int p = partition(arr, lo, hi);
        quicksort(arr, lo, p - 1);
        quicksort(arr, p + 1, hi);
    }
}

int main(void) {
    srand(time(NULL));
    size_t size;
    puts("size of array?");
    scanf("%d", &size);
    int* arr = alloca(size * sizeof(int));
    for (int i = 0; i < size; ++i) arr[i] = rand() % size;

    quicksort(arr, 0, size - 1);

    for (int i = 0; i < size; ++i) printf("%d, ", arr[i]);
    puts("");
}