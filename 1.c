#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int miesiac;
    int dzien;
} Data;

typedef struct {
    int rzad;
    char kolumna;
    char rezerwujacy[256];
    Data data;
} Rezerwacja;

Rezerwacja* sczytajRezerwacje() {
    Rezerwacja* rez = malloc(sizeof(Rezerwacja));
    if (rez == NULL) {
        puts("Blad alokacji pamieci");
        exit(1);
    }
    puts("rzad:");
    scanf("%d", &rez->rzad);
    puts("kolumna:");
    scanf("%c", &rez->kolumna);
    puts("rezerwujacy:");
    scanf("%d", rez->rezerwujacy);
    puts("dzien:");
    scanf("%d", &rez->data.dzien);
    puts("miesiac:");
    scanf("%d", &rez->data.miesiac);

    return rez;
}

void drukujRezerwacje(Rezerwacja* rez) {
    printf(
        "Rezerwacja w rzedzie %d, kolumnie %c \n",
        rez->rzad, rez->kolumna
    );
    printf(
        "rezerwujacy: %s\nmiesiac %d, dzien %d",
        rez->rezerwujacy, rez->data.miesiac, rez->data.dzien
    );
}

int main(void) {
    Rezerwacja* rez = sczytajRezerwacje();
    drukujRezerwacje(rez);
    free(rez);
}