#include <stdio.h>
#include <stdlib.h>

struct sll_node {
  int data;
  struct sll_node *next;
};

void fill(struct sll_node **n) {
  *n = (struct sll_node *)malloc(sizeof(struct sll_node));
  (*n)->next = (struct sll_node *)malloc(sizeof(struct sll_node));
  (*n)->next->next = (struct sll_node *)malloc(sizeof(struct sll_node));
  (*n)->next->next->next = (struct sll_node *)malloc(sizeof(struct sll_node));
  (*n)->next->next->next->next =
      (struct sll_node *)malloc(sizeof(struct sll_node));
  (*n)->next->next->next->next->next = NULL;
  (*n)->data = 2;
  (*n)->next->data = 3;
  (*n)->next->next->data = 4;
  (*n)->next->next->next->data = 5;
  (*n)->next->next->next->next->data = 6;
}

void print_list(struct sll_node *node)
{
  if (NULL != node)
  {
    printf("%d ", node->data);
    print_list(node->next);
  }
  else
    printf("\n");
}

void insert(struct sll_node** list, int val) {
    if (val < (**list).data) {
        struct sll_node* new = malloc(sizeof(struct sll_node));
        new->next = *list;
        new->data = val;
        *list = new;
        return;
    }
    struct sll_node* current = *list;
    while (current->next != NULL && current->next->data < val) {
        current = current->next;
    }
    struct sll_node* tmp = current->next;
    current->next = malloc(sizeof(struct sll_node));
    current = current->next;
    current->data = val;
    current->next = tmp;
}

int main(void) {
    struct sll_node* node;
    fill(&node);
    print_list(node);

    insert(&node, 1);
    print_list(node);

    insert(&node, 10);
    print_list(node);

    insert(&node, 4);
    print_list(node);
}